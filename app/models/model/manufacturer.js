/*
* @Author: xiaoxiaolai
* @Date:   2017-05-01 19:29:54
* @Last Modified by:   xiaoxiaolai
* @Last Modified time: 2017-05-01 20:06:48
*/
const mongoose = require('../../../config/db/mongoose.js')();
const Manufacturer = mongoose.model('manufacturer');


// const updateUser = async (user) => {
// 	if(user._id&&(typeof user._id == 'string')){
//            return User.update({_id:user._id}, user).exec();
//         }
// };

// const deleteUser = async (id) => {
//     return User.remove({_id:id}).exec();
// };

// const insertUser = async (user) => {
//     return User.create(user);
// };

const selectManufacturer = async () => {
	 // Manufacturer.create({name:"koa"}).then((res)=>{
	 // 	console.log("res")
	 // 	console.log(res)
	 // })
    return Manufacturer.find({}).exec();
};

// const selectUserByPhone = async (phone) => {
//     return User.find({ phone: phone }).exec();
// };

// const login = async (ctx, next) => {

//     // header("Access-Control-Allow-Origin: *")
//     console.log("ctx.request.body")
//     console.log(ctx.request.body)
//     res.body="login";
//     res.code="200";
//     // ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.body= res; 
// };



export default {
    selectManufacturer
};
