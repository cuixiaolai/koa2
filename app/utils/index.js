const generateCode = () => {
    let items = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPRSTUVWXYZ23456789'.split('');
    let randInt = function (start, end) {
        return Math.floor(Math.random() * (end - start)) + start;
    }
    let code = '';
    for (let i = 0; i < 4; ++i) {
        code += items[randInt(0, items.length)];
    }
    return code;
}

const checkEmail = (email) =>{
    let filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(email)) return true;
    else return false;
}

const checkPhone = (phone) =>{
    let filter  = /^1(3|4|5|7|8)\d{9}$/;
    if (filter.test(phone)) return true;
    else return false;
}
export default {
	generateCode,
    checkEmail,
    checkPhone
};