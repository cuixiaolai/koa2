import router from './routes/index.js';
const Koa = require('koa');
const app = new Koa();
const serve = require('koa-static');
const views = require('koa-views');
const body = require('koa-json-body');
// const bodyParser = require('koa-bodyparser');
const kcors = require('kcors');
const session = require("koa-session2");

//req time
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});



//session
app.use(session({
    key: "SESSIONID",   //default "koa:sess"
}));

//cross-domain req
app.use(kcors({origin:"http://localhost:8888",credentials:true,allowHeaders:"Origin, X-Requested-With, Content-Type, Accept, If-Modified-Since"}));

//err
app.use(async (ctx, next) => {
  try {
    await next() 
  } catch (err) {
    ctx.body = { message: err.message }
    ctx.status = err.status || 500
  }
})

//body parse
app.use(body({ limit: '10kb', fallback: true }))
// app.use(bodyParser())

//static files
app.use(serve(__dirname + '/../public'));

//views
app.use(views(__dirname + '/../public/views'));



// router
app.use(router.routes()).use(router.allowedMethods());

//error
//todo errorPage
app.on('error', function(err){
  log.error('server error', err);
});

app.on('error', function(err, ctx){
  log.error('server error', err, ctx);
});

app.listen(3001);