import user from  '../models/model/user.js';
import action from  '../action/user.js';
const res = {};

const updateUser = async (ctx, next) => {
     await user.updateUser({_id:"58eb2562627d93073152f627",name:"ddddddd"})
     .then((result)=>{
        if(result.ok&&result.ok>0){
            res.code = "200";
            res.body = "updateUser";
        }
     })
     .catch((err)=>{
        if(err){
            res.code = "400";
            res.body = "updateUser";
        }
     })
     ctx.body = res;  
};

const deleteUser = async (ctx, next) => {
     await user.deleteUser("58ed972fb14f012552deab43")
     .then((result)=>{
        if(JSON.parse(result).n>0){
            res.code = "200";
            res.body = "updateUser";
        }else{
            res.code = "400";
            res.body = "updateUser";
        }
     })
     .catch((err)=>{
        if(err){
            res.code = "400";
            res.body = "updateUser";
        }
     })
     ctx.body = res;  
};

const insertUser = async (ctx, next) => {
    console.log(ctx.request.body.type)
    console.log(ctx.request.body.name)
    console.log(ctx.request.body.pwd)
    let name = ctx.request.body.name;
    let pwd = ctx.request.body.pwd;
    let type = ctx.request.body.type;
    if(type=="email"){
     await user.insertUser({email:name,pwd:pwd})
     .then((result)=>{
        console.log("result")
        console.log(result)
        if(result){
            res.code = "200";
            res.body = result;
        }
     })
     .catch((err)=>{
        console.log("err")
        console.log(err)
        if(err){
            res.code = "400";
            res.body = "insertUser";
        }
     })
 }else if(type=="phone"){
      await user.insertUser({phone:name,pwd:pwd})
     .then((result)=>{
        console.log("result")
        console.log(result)
        if(result){
            res.code = "200";
            res.body = result;
        }
     })
     .catch((err)=>{
        console.log("err")
        console.log(err)
        if(err){
            res.code = "400";
            res.body = "insertUser";
        }
     })

 }else{
    res.code = "400";
    res.body = "insertUser";
 }

     ctx.body = res;  
};

const selectUser = async (ctx, next) => {
    console.log("ctx.request.body")
    console.log(ctx.request.body)
     await user.selectUserByEmail("616646651@qq.com")
     .then((result)=>{
        console.log(result);
        if(result&&result.length>0){
            res.code = "200";
            res.body = result;
        }else{
            res.code = "400";
            res.body = "result";
        }
     })
     .catch((err)=>{
        console.log("err")
        console.log(err)
        if(err){
            res.code = "400";
            res.body = "selectUser";
        }
     })
     // ctx.set('Content-Type', 'text/json');
     // ctx.set('CharacterEncoding', 'UTF-8');
     // let user = ctx.session.user;
     // if(user){
     //    res.body = user;
     //    res.code = 200;
     // }else{
     //    res.body = "notLogin";
     //    res.code = 400;
     // }
     ctx.body = res;  
};

const login = async (ctx, next) => {
    console.log("login");
    console.log(ctx.request.body);
    // pwd;name
    const pwd = ctx.request.body.pwd;
    const name = ctx.request.body.name;
    const checkUser = await action.checkUserExist(name);
    console.log(checkUser);
    console.log(checkUser.exist);
    if(checkUser.exist){
        console.log("exist")
        console.log(checkUser.user)
        console.log(checkUser.user.pwd)
        if(pwd==checkUser.user.pwd){
            ctx.session.user = checkUser.user;
            ctx.session.date = Date.now();
            console.log(ctx.session.date);
            res.code = "200";
            res.body = "loginSuccess";
        }else{
            res.code = "400";
            res.body = "密码错误";
        }

    }else{
        res.code = "400";
        res.body = "账户不存在";
    }
    ctx.body= res; 
};

const checkUserExist = async (ctx, next) => {
    const value = ctx.request.body.value;
    const checkUser = await action.checkUserExist(value);
    if(checkUser.exist){
        res.code = "200";
        res.body = "账户存在";
    }else{
        res.code = "400";
        res.body = "账户不存在";
    }
    ctx.body= res; 
};



export default {
    updateUser,
    deleteUser,
    insertUser,
    selectUser,
    login,
    checkUserExist
};