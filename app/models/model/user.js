const mongoose = require('../../../config/db/mongoose.js')();
const User = mongoose.model('user');
const p = require('bluebird');
const Promise = global.Promise;

const updateUser = async (user) => {
	if(user._id&&(typeof user._id == 'string')){
           return User.update({_id:user._id}, user).exec();
        }
};

const deleteUser = async (id) => {
    return User.remove({_id:id}).exec();
};

const insertUser = async (user) => {
    return User.create(user);
};

const selectUserByEmail = async (email) => {
    return User.find({ email: email }).exec();
};

const selectUserByPhone = async (phone) => {
    return User.find({ phone: phone }).exec();
};

const login = async (ctx, next) => {

    // header("Access-Control-Allow-Origin: *")
    console.log("ctx.request.body")
    console.log(ctx.request.body)
    res.body="login";
    res.code="200";
    // ctx.set('Access-Control-Allow-Origin', '*');
    ctx.body= res; 
};



export default {
    updateUser,
    deleteUser,
    insertUser,
    selectUserByEmail,
    selectUserByPhone,
    login
};
