/*
* @Author: xiaoxiaolai
* @Date:   2017-05-01 19:32:48
* @Last Modified by:   xiaoxiaolai
* @Last Modified time: 2017-05-01 19:42:24
*/

import Router from 'koa-router';
import manufacturer from '../controllers/manufacturer';
import check from '../service/checkLogin/checkLogin';

const router = Router({
  prefix: '/api/v1'
});

// router.get('/user', check.checkLogin,manufacturer.selectManufacturer);	
router.get('/manufacturer', manufacturer.selectManufacturer);	


module.exports = router;
