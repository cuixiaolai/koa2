var mongoose = require('mongoose');
var Promise1 = require('bluebird');
// mongoose.Promise = Promise1;
mongoose.Promise = global.Promise;
var config = require('./config.js');

module.exports = function() {
	var host = config.mongodbConfig.host;
	var port = config.mongodbConfig.port;
	var dbname = config.mongodbConfig.database;
	var connectUrl = 'mongodb://' + host + ':'+ port +'/' + dbname;
	var options = {
          // db: { native_parser: true },        
          server: {
            poolSize: 200,
            auto_reconnect: true,
            keepAlive: 100,
            socketOptions: {
                socketTimeoutMS: 0,
                connectionTimeout: 0
              }
          }
    }

		mongoose.connect(connectUrl,options);
		var db = mongoose.connection;
		db.on('error', function(){
			console.log("connect");
		});

	

	require('../../app/models/schema/user.js');
	require('../../app/models/schema/manufacturer.js');

	return db;
}
