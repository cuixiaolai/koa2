import Router from 'koa-router';
import phone from '../controllers/phone';

const router = Router({
  prefix: '/api/v1'
});
	
router.get('/phone/:phone', phone.sendPhoneCode);	 
router.post('/phone', phone.checkPhoneCode);	
// router.options('/phone',  ctx=>{
// 	console.log(ctx)
//     ctx.set('Access-Control-Allow-Method', 'POST');
//     ctx.set('Access-Control-Allow-Origin', 'http://localhost:8888');
//     ctx.status = 204;
// });

module.exports = router;
