const TopClient = require('./topClient').TopClient;
import utils from  '../../utils/index.js';
var client = new TopClient({
    'appkey': '23746576',
    'appsecret': 'a1263386d805bdf6687b1b9db091ca1f',
    'REST_URL': 'http://gw.api.taobao.com/router/rest'
});

const promise = global.Promise;

const sendPhoneCode = (phone)=>{
    const code = utils.generateCode().toLowerCase();
    console.log("code")
    console.log(code)
    return new promise ((resolve, reject)=>{
    client.execute('alibaba.aliqin.fc.sms.num.send', {
    'extend':'',
    'sms_type':'normal',
    'sms_free_sign_name':'icxyz商城',
    'sms_param':"{randomCode:\'"+code+"\'}",
    'rec_num':phone,
    'sms_template_code':'SMS_61760222'
}, function(error, response) {
    if(error){
        console.log("error")
        console.log(error)
        return reject(error);
    }
    response.code = code;
    console.log("response")
    console.log(response)
    return resolve(response)
})
})

}
export default {
    sendPhoneCode
};