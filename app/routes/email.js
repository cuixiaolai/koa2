import Router from 'koa-router';
import email from '../controllers/email';

const router = Router({
  prefix: '/api/v1'
});
	
router.get('/email', email.sendEmail);	
router.post('/email', email.checkEmail);	

module.exports = router;
