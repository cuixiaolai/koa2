import veifyCode from  '../service/veifyCode/veifyCode.js';
const res = {};

const code = async (ctx, next) => {
	const result = await veifyCode.Generate(ctx.params.veifyCode);
	console.log(result.code) 
	if(result){
		ctx.session.veifyCode = result.code;
		res.code = "200";
		res.body = result.dataURL;
	}else{
		ctx.session.veifyCode = "";
		res.code = "400";
		res.body = "veifyCodeErr"
	}
    ctx.body = res;  
};

const veify = async (ctx, next) => {
	console.log(ctx.request.body.code);
	console.log(ctx.session.veifyCode);
    const code = ctx.request.body.code.toLowerCase();
	if(code&&code==ctx.session.veifyCode){
		res.code = "200";
		res.body = "veify";
	}else{
		res.code = "400";
		res.body = "veify";
	}

    ctx.body = res;  
};

const test = async (ctx, next) => {
	console.log(ctx)
	console.log(ctx.params)
	console.log(JSON.parse(JSON.stringify("ctx.params.veifyCode")))
	console.log(JSON.parse(JSON.stringify(ctx.params.veifyCode)))
	console.log(JSON.parse(JSON.stringify(ctx.params.veifyCode)))
	console.log(JSON.stringify(ctx.params.veifyCode))
	console.log(ctx.query.veifyCode)


    ctx.body = ctx;  
};

export default {
	code,
	veify,
	test
};