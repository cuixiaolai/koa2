
import Router from 'koa-router';
import user from '../controllers/user';
import check from '../service/checkLogin/checkLogin';

const router = Router({
  prefix: '/api/v1'
});
// router.use('/user', check.checkLogin);
router.put('/user', check.checkLogin,user.updateUser);	
router.delete('/user', user.deleteUser);	
router.post('/user', user.insertUser);	
router.get('/user', user.selectUser);	
router.post('/login', user.login);	
router.post('/checkUser', user.checkUserExist);	

// router.options('/login',  ctx=>{
// 	console.log(ctx)
//     ctx.set('Access-Control-Allow-Method', 'POST');
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.status = 204;
// });

module.exports = router;
