/*
* @Author: xiaoxiaolai
* @Date:   2017-05-01 19:31:55
* @Last Modified by:   xiaoxiaolai
* @Last Modified time: 2017-05-01 20:05:44
*/
const mongoose = require('mongoose');

const manufacturer = new mongoose.Schema({
	name: String,
},{collection:"manufacturer"});
mongoose.model('manufacturer', manufacturer);