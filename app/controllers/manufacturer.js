/*
* @Author: xiaoxiaolai
* @Date:   2017-05-01 19:37:59
* @Last Modified by:   xiaoxiaolai
* @Last Modified time: 2017-05-01 20:07:20
*/
import manufacturer from  '../models/model/manufacturer.js';
import action from  '../action/user.js';
const res = {};

const selectManufacturer = async (ctx, next) => {
     await manufacturer.selectManufacturer()
     .then((result)=>{
     	console.log("result")
     	console.log(result)
        if(result&&result.length>0){
            res.code = "200";
            res.body = result;
        }
     })
     .catch((err)=>{
        if(err){
            res.code = "400";
            res.body = "updateUser";
        }
     })
     ctx.body = res;  
};

export default {
    selectManufacturer
};