const mongoose = require('mongoose');

const user = new mongoose.Schema({
	name: String,
	pwd: String,
	phone: String,
	email: String
});
mongoose.model('user', user);