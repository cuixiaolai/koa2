import email from '../service/email/email.js';

const res = {};

const sendEmail = async (ctx, next) => {
	console.log(ctx.query.email);
	const to = ctx.query.email;
	await email.sendEmail(to).then(result =>{
		if(result){
			ctx.session.emailCode = result.code;
			console.log("ctx.session.emailCode")
			console.log(ctx.session.emailCode)
			res.code = "200";
			res.body = "sendEmail";
		}
	}).catch(err => {
		if(err){
			res.code = "400";
			res.body = "sendEmail"
		}
	})
    ctx.body = res;  
};

const checkEmail = async (ctx, next) => {
	console.log(ctx.request.body.emailCode);
	console.log(ctx.session.emailCode);
	if(ctx.request.body.emailCode){
    const code = ctx.request.body.emailCode.toLowerCase();
	if(code&&code==ctx.session.emailCode){
		res.code = "200";
		res.body = "checkEmail";
	}else{
		res.code = "400";
		res.body = "checkEmail";
	}
    }else{
    	res.code = "400";
    	res.body = "checkEmail";
    }
    ctx.body = res;  
};

export default {
	sendEmail,
	checkEmail
};