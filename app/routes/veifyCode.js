import Router from 'koa-router';
import veifyCode from '../controllers/veifyCode';

const router = Router({
  prefix: '/api/v1'
});
	
router.get('/veifyCode', veifyCode.code);	
router.get('/veifyCode/:veifyCode', veifyCode.code);	
router.post('/veifyCode', veifyCode.veify);	

module.exports = router;
